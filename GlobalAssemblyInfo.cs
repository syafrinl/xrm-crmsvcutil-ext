﻿using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("Syafrin Liong")]
[assembly: AssemblyProduct("SL.Xrm.Util.CrmSvcUtilExt")]
[assembly: AssemblyCopyright("Copyleft ©  2015")]
[assembly: AssemblyTrademark("")]

//[assembly: AssemblyFileVersion("1.0.0.0")]