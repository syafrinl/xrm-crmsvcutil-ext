﻿using Microsoft.Xrm.Sdk.Query;
using NUnit.Framework;
using SL.Xrm.Util.CrmSvcUtilExt.Filter;

namespace SL.Xrm.Util.CrmSvcUtilExt.Test.Int.Filter
{
    [TestFixture]
    public class CrmOrganizationServiceFactoryTest
    {
        [Test]
        public void Create()
        {
            //Act
            var service = CrmOrganizationServiceFactory.Create();
           
            var contacts = service.RetrieveMultiple(new QueryExpression("contact"));

            //Assert
            Assert.NotNull(contacts);
            Assert.Greater(contacts.Entities.Count, 0);
        }
    }
}
