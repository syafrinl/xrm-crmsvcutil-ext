﻿using System.Linq;
using Microsoft.Xrm.Sdk;

namespace SL.Xrm.Util.CrmSvcUtilExt.Model
{
    public partial class XrmServiceContext
    {
        public new SaveChangesResultCollection SaveChanges()
        {
            var attachedEntities = GetAttachedEntities().ToList();
            foreach (var attachedEntity in attachedEntities)
            {
                var dirtyEntity = attachedEntity as DirtyAttributeTrackableEntity;
                if (dirtyEntity != null)
                {
                    if (dirtyEntity.EntityState == EntityState.Changed)
                    {
                        Detach(dirtyEntity);
                        UpdateObject(dirtyEntity.GetEntityToSave());
                    }
                    dirtyEntity.ClearDirtyAttributesTracking(); //Clear Dirty Attributes for Create and Update
                }
            }

            var result = base.SaveChanges();

            return result;
        }

        public new void UpdateObject(Entity entity)
        {
            Attach(entity);

            base.UpdateObject(entity);
        }

        public new void DeleteObject(Entity entity)
        {
            Attach(entity);

            base.DeleteObject(entity);
        }

        public new void Attach(Entity entity)
        {
            if (!IsAttached(entity))
            {
                entity.EntityState = null;

                base.Attach(entity);
            }
        }
    }
}
