﻿using System;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace SL.Xrm.Util.CrmSvcUtilExt.Model
{
    public abstract class DirtyAttributeTrackableEntity : Entity
    {
        protected List<string> DirtyAttributes = new List<string>();

        protected DirtyAttributeTrackableEntity(string entityLogicalName) : base(entityLogicalName)
        {
        }

        public Entity GetEntityToSave()
        {
            var entityToSave = new Entity
            {
                Id = Id,
                LogicalName = LogicalName
            };

            foreach (var dirtyAttribute in DirtyAttributes)
            {
                if (dirtyAttribute.Equals(LogicalName + "Id", StringComparison.OrdinalIgnoreCase))
                    continue;

                entityToSave.Attributes.Add(dirtyAttribute, Attributes[dirtyAttribute]);
            }

            return entityToSave;
        }

        public void ClearDirtyAttributesTracking()
        {
            DirtyAttributes.Clear();
        }
    }
}
