﻿using System;
using Microsoft.Xrm.Sdk;

namespace SL.Xrm.Util.CrmSvcUtilExt.Model
{
    public partial interface IXrmServiceContext : IDisposable
    {
        #region Data Operations
        void AddObject(Entity entity);
        void DeleteObject(Entity entity);
        void UpdateObject(Entity entity);
        SaveChangesResultCollection SaveChanges();
        #endregion
    }
}
