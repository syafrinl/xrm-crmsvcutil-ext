﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Configuration;
using NUnit.Framework;
using System;
using System.Linq;

namespace SL.Xrm.Util.CrmSvcUtilExt.Model.Test.Int
{
    [TestFixture]
    public class SaveChangesTest
    {
        private IXrmServiceContext ServiceContext { get; set; }

        [SetUp]
        public void SetUp()
        {
            var crmConnection = new CrmConnection("Xrm");

            ServiceContext = new XrmServiceContext(CrmConfigurationManager.CreateService(crmConnection));
        }

        [Test]
        public void SaveChanges()
        {
            var retrieveContact = ServiceContext.ContactSet.First();

            retrieveContact.FirstName = "Sally";
            retrieveContact.BirthDate = new DateTime(1934,3,4);
            ServiceContext.UpdateObject(retrieveContact);
            //ServiceContext.SaveChanges();

            retrieveContact.LastName = "Amyrr";
            ServiceContext.UpdateObject(retrieveContact);
            ServiceContext.SaveChanges();
        }

        [Test]
        public void SaveChanges2()
        {
            var newContact = new Contact
            {
                FirstName = "Matt"
            };
            ServiceContext.AddObject(newContact);
            ServiceContext.SaveChanges();

            newContact.LastName = "Lee";
            newContact.BirthDate = new DateTime(1922, 2, 2);
            ServiceContext.UpdateObject(newContact);
            ServiceContext.SaveChanges();

            newContact.LastName = "Jones";
            ServiceContext.UpdateObject(newContact);
            ServiceContext.SaveChanges();

            ServiceContext.DeleteObject(newContact);
            ServiceContext.SaveChanges();
        }
    }
}
