﻿using System.ServiceModel.Description;
using NUnit.Framework;
using SL.Xrm.Util.CrmSvcUtilExt.Filter;

namespace SL.Xrm.Util.CrmSvcUtilExt.Test.Unit.Filter
{
    [TestFixture]
    public class CrmOrganizationServiceFactoryTest
    {
        [Test]
        public void Create()
        {
            //Act
            var service = CrmOrganizationServiceFactory.Create();

            //Assert
            Assert.NotNull(service);
        }

        [Test]
        public void CreateConnection()
        {
            //Act
            var connection = CrmOrganizationServiceFactory.CreateConnection();

            //Assert
            Assert.NotNull(connection);
            Assert.AreEqual(@"http://crmserver.myorg.com/myorg/", connection.ServiceUri.ToString());
            Assert.AreEqual("myorg", ((ClientCredentials)connection.ClientCredentials).Windows.ClientCredential.Domain);
            Assert.AreEqual("crmuser", ((ClientCredentials)connection.ClientCredentials).Windows.ClientCredential.UserName);
            Assert.AreEqual("pwd1", ((ClientCredentials)connection.ClientCredentials).Windows.ClientCredential.Password);
        }

        [Test]
        public void CreateConnection_CrmOnline()
        {
            //Arrange
            var url = @"https://myorg.crm.dynamics.com/";
            var username = "crmuser@myorg.onmicrosoft.com";
            var password = "pwd1";

            //Act
            var connection = CrmOrganizationServiceFactory.CreateConnection(url, null, username, password);

            //Assert
            Assert.NotNull(connection);
            Assert.AreEqual(url, connection.ServiceUri.ToString());
            Assert.AreEqual(username, ((ClientCredentials)connection.ClientCredentials).UserName.UserName);
            Assert.AreEqual(password, ((ClientCredentials)connection.ClientCredentials).UserName.Password);
        }

        [Test]
        public void CreateConnection_CrmOnPremise()
        {
            //Arrange
            var url = @"http://crmserver.myorg.com/myorg/";
            var domain = "myorg";
            var username = "crmuser";
            var password = "pwd1";
            
            //Act
            var connection = CrmOrganizationServiceFactory.CreateConnection(url, domain, username, password);
            
            //Assert
            Assert.NotNull(connection);
            Assert.AreEqual(url, connection.ServiceUri.ToString());
            Assert.AreEqual(domain, ((ClientCredentials)connection.ClientCredentials).Windows.ClientCredential.Domain);
            Assert.AreEqual(username, ((ClientCredentials)connection.ClientCredentials).Windows.ClientCredential.UserName);
            Assert.AreEqual(password, ((ClientCredentials)connection.ClientCredentials).Windows.ClientCredential.Password);
        }
    }
}
