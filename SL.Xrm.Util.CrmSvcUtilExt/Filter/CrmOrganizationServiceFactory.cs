﻿using System.Configuration;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;

namespace SL.Xrm.Util.CrmSvcUtilExt.Filter
{
    public static class CrmOrganizationServiceFactory
    {
        public static IOrganizationService Create()
        {
            return new CrmOrganizationServiceContext(CreateConnection());
        }

        internal static CrmConnection CreateConnection()
        {
            var url = ConfigurationManager.AppSettings["url"].Replace("XRMServices/2011/Organization.svc", "");
            var domain = ConfigurationManager.AppSettings["domain"];
            var username = ConfigurationManager.AppSettings["username"];
            var password = ConfigurationManager.AppSettings["password"];

            return CreateConnection(url, domain, username, password);
        }

        internal static CrmConnection CreateConnection(string url, string domain, string username, string password)
        {
            return CrmConnection.Parse($"Url={url};{(!string.IsNullOrEmpty(domain)?$"Domain={domain};":"")}Username={username};Password={password};");
        }
    }
}
