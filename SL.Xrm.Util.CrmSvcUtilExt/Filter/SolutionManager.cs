﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using SL.Xrm.Util.CrmSvcUtilExt.Comparer;

namespace SL.Xrm.Util.CrmSvcUtilExt.Filter
{
    public class SolutionManager
    {
        private IOrganizationService CrmService { get; }
        private IEnumerable<string> SolutionNames { get; }
        private IEnumerable<string> RequiredEntityNames { get; }

        private IEnumerable<Guid> _entityIds;
        public IEnumerable<Guid> EntityIds => _entityIds ?? (_entityIds = RetrieveEntityIds());

        private IEnumerable<string> _optionSetNames;
        public IEnumerable<string> OptionSetNames => _optionSetNames ?? (_optionSetNames = RetrieveOptionSetNames());
        

        public SolutionManager(IOrganizationService crmService, string solutionNames, string requiredEntityNames)
        {
            CrmService = crmService;
            SolutionNames = solutionNames.Split(',');
            RequiredEntityNames = requiredEntityNames.Split(',');
        }


        internal IEnumerable<Guid> RetrieveEntityIds()
        {
            IEnumerable<Guid> requiredEntityIds = new Guid[0];
            IEnumerable<Guid> solutionEntityIds = new Guid[0];

            #region Solution Entities
            if (SolutionNames.Any() && !string.IsNullOrEmpty(SolutionNames.First()))
            {
                var solutionLink = new LinkEntity
                {
                    JoinOperator = JoinOperator.Inner,
                    LinkFromAttributeName = "solutionid",
                    LinkFromEntityName = "solutioncomponent",
                    LinkToAttributeName = "solutionid",
                    LinkToEntityName = "solution"
                };

                solutionLink.LinkCriteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions =
                    {
                        new ConditionExpression("uniquename", ConditionOperator.In, SolutionNames.ToArray())
                    }
                };

                var query = new QueryExpression
                {
                    Distinct = false,
                    EntityName = "solutioncomponent",
                    ColumnSet = new ColumnSet(true),
                    LinkEntities = {solutionLink},
                    Criteria =
                    {
                        Filters =
                        {
                            new FilterExpression
                            {
                                FilterOperator = LogicalOperator.Or,
                                Conditions =
                                {
                                    new ConditionExpression("componenttype", ConditionOperator.Equal, 1), //Entity
                                },
                            }
                        }
                    }
                };

                var ec = CrmService.RetrieveMultiple(query);
                solutionEntityIds = ec.Entities.Select(e => (Guid) e.Attributes["objectid"]).Distinct();
            }
            #endregion

            #region Required entities
            if (RequiredEntityNames.Any() && !string.IsNullOrEmpty(RequiredEntityNames.First()))
            {
                var allEntitiesRequest = new RetrieveAllEntitiesRequest
                {
                    EntityFilters = EntityFilters.Entity,
                    RetrieveAsIfPublished = true
                };
                var allEntitiesResponse = (RetrieveAllEntitiesResponse)CrmService.Execute(allEntitiesRequest);

                requiredEntityIds = allEntitiesResponse
                                        .EntityMetadata
                                        .Where(x => x.MetadataId.HasValue && RequiredEntityNames.Contains(x.SchemaName))
                                        .Select(x => x.MetadataId.Value);
            }
            #endregion

            var entityIds = solutionEntityIds.Union(requiredEntityIds).Distinct().ToList();

            #region Many to many Relationships
            var manyToManyRequest = new RetrieveAllEntitiesRequest
            {
                EntityFilters = EntityFilters.Relationships,
                RetrieveAsIfPublished = true
            };

            var manyToManyResponse = (RetrieveAllEntitiesResponse)CrmService.Execute(manyToManyRequest);

            var manyToManyRelationshipIds = manyToManyResponse.EntityMetadata
                .Join(entityIds, em => em.MetadataId, e => e, (metadata, id) => metadata)
                .SelectMany(m => m.ManyToManyRelationships.Where(c => c.MetadataId.HasValue).Select(c => c.MetadataId.Value));
            #endregion

            return entityIds.Union(manyToManyRelationshipIds);
        }
        
        internal IEnumerable<string> RetrieveOptionSetNames()
        {
            var request = new RetrieveAllEntitiesRequest
            {
                EntityFilters = EntityFilters.Attributes | EntityFilters.Relationships
            };

            var response = (RetrieveAllEntitiesResponse)CrmService.Execute(request);

            var picklists = response.EntityMetadata
                                    .Join(EntityIds, em => em.MetadataId, e => e, (metadata, id) => metadata)
                                    .SelectMany(m => m.Attributes.Where(a => a.AttributeType == AttributeTypeCode.Picklist)
                                                      .Cast<PicklistAttributeMetadata>()
                                                      .Select(a => a.OptionSet))
                                    .Distinct();

            var statuses = response.EntityMetadata
                                    .Join(EntityIds, em => em.MetadataId, e => e, (metadata, id) => metadata)
                                    .SelectMany(m => m.Attributes.Where(a => a.AttributeType == AttributeTypeCode.Status)
                                                      .Cast<StatusAttributeMetadata>()
                                                      .Select(a => a.OptionSet))
                                    .Distinct();

            return picklists.Union(statuses).Distinct(new OptionSetComparer()).Select(x => x.Name);
        }
    }
}
