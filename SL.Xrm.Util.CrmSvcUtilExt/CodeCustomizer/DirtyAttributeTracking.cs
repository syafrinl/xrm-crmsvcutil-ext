﻿using System;
using System.CodeDom;

namespace SL.Xrm.Util.CrmSvcUtilExt.CodeCustomizer
{
    public static class DirtyAttributeTracking
    {
        //DirtyAttributes is defined in DirtyAttributeTrackableEntity class
        private static readonly string DirtyAttributeListName = "DirtyAttributes";

        public static void AddAttributeTracking(CodeTypeDeclaration entityTypeDeclaration)
        {
            foreach (CodeTypeMember member in entityTypeDeclaration.Members)
            {
                var method = member as CodeMemberMethod;
                if (method != null)
                {
                    AddChangedAttributeTrackingStatement(method);
                }
            }
        }

        private static void AddChangedAttributeTrackingStatement(CodeMemberMethod method)
        {
            if (string.Equals(method.Name, "OnPropertyChanged", StringComparison.OrdinalIgnoreCase))
            {
                var attributeNameToLower = new CodeVariableDeclarationStatement(typeof(string), 
                    "attributeName",
                    new CodeMethodInvokeExpression(new CodeVariableReferenceExpression(method.Parameters[0].Name), "ToLower"));
                method.Statements.Add(attributeNameToLower);

                var ifContainsExpression = new CodeBinaryOperatorExpression(
                    new CodeMethodInvokeExpression(
                        new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), DirtyAttributeListName),
                        "Contains",
                        new CodeVariableReferenceExpression("attributeName")),
                    CodeBinaryOperatorType.ValueEquality,
                    new CodePrimitiveExpression(false)
                    );

                var addChangeExpression = new CodeMethodInvokeExpression(
                    new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), DirtyAttributeListName),
                    "Add",
                    new CodeVariableReferenceExpression("attributeName")
                    );

                var changeExpression = new CodeConditionStatement(ifContainsExpression,
                    new CodeExpressionStatement(addChangeExpression));

                method.Statements.Add(changeExpression);
            }
        }
    }
}
