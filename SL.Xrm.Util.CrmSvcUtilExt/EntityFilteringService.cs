﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Microsoft.Crm.Services.Utility;
using Microsoft.Xrm.Sdk.Metadata;
using SL.Xrm.Util.CrmSvcUtilExt.Filter;

namespace SL.Xrm.Util.CrmSvcUtilExt
{
    public sealed class EntityFilteringService : ICodeWriterFilterService
    {
        private ICodeWriterFilterService DefaultService { get; set; }

        private readonly IList<Guid> _entityIds;
        
        private static readonly string SolutionNames = ConfigurationManager.AppSettings["solutionNames"];
        private static readonly string RequiredEntityNames = ConfigurationManager.AppSettings["requiredEntities"];

        public EntityFilteringService(ICodeWriterFilterService defaultService)
        {
            Trace.TraceInformation("Loading Filters...");

            DefaultService = defaultService;

            var crmService = CrmOrganizationServiceFactory.Create();
            var solutionFilter = new SolutionManager(crmService, SolutionNames, RequiredEntityNames);

            _entityIds = solutionFilter.EntityIds.ToList();
        }

        bool ICodeWriterFilterService.GenerateAttribute(AttributeMetadata attributeMetadata, IServiceProvider services)
        {
            return DefaultService.GenerateAttribute(attributeMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateEntity(EntityMetadata entityMetadata, IServiceProvider services)
        {
            if (entityMetadata.MetadataId.HasValue && _entityIds.Contains(entityMetadata.MetadataId.Value) == false)
            {
                return false;
            }

            return DefaultService.GenerateEntity(entityMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateOption(OptionMetadata optionMetadata, IServiceProvider services)
        {
            return DefaultService.GenerateOption(optionMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateOptionSet(OptionSetMetadataBase optionSetMetadata, IServiceProvider services)
        {
            return DefaultService.GenerateOptionSet(optionSetMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateRelationship(RelationshipMetadataBase relationshipMetadata, EntityMetadata otherEntityMetadata, IServiceProvider services)
        {
            return DefaultService.GenerateRelationship(relationshipMetadata, otherEntityMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateServiceContext(IServiceProvider services)
        {
            return DefaultService.GenerateServiceContext(services);
        }
    }
}

