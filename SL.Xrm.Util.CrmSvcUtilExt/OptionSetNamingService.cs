﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Crm.Services.Utility;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace SL.Xrm.Util.CrmSvcUtilExt
{
    public sealed class OptionSetNamingService : INamingService
    {
        private INamingService DefaultNamingService { get; set; }
        
        private Dictionary<OptionSetMetadataBase, Dictionary<string, int>> _definedOptionSetNames;

        public OptionSetNamingService(INamingService namingService)
        {
            DefaultNamingService = namingService;
            _definedOptionSetNames = new Dictionary<OptionSetMetadataBase, Dictionary<string, int>>();
        }

        #region INamingService methods
        /// <summary>
        /// Provide a new implementation for finding a name for an OptionSet. If the
        /// OptionSet is not global, we want the name to be the concatenation of the Entity's
        /// name and the Attribute's name.  Otherwise, we can use the default implementation.
        /// </summary>
        public string GetNameForOptionSet(EntityMetadata entityMetadata, OptionSetMetadataBase optionSetMetadata, IServiceProvider services)
        {
            // Ensure that the OptionSet is not global before using the custom
            // implementation.
            if (optionSetMetadata.IsGlobal.HasValue && !optionSetMetadata.IsGlobal.Value)
            {
                // Find the attribute which uses the specified OptionSet.
                var attribute =
                    (from a in entityMetadata.Attributes
                     where a.AttributeType == AttributeTypeCode.Picklist
                        && ((EnumAttributeMetadata)a).OptionSet.MetadataId == optionSetMetadata.MetadataId
                     select a).FirstOrDefault();

                // Check for null, since statuscode attributes on custom entities are not
                // global, but their optionsets are not included in the attribute
                // metadata of the entity, either.
                if (attribute != null)
                {
                    // Concatenate the name of the entity and the name of the attribute
                    // together to form the OptionSet name.
                    return $"{DefaultNamingService.GetNameForEntity(entityMetadata, services)}{DefaultNamingService.GetNameForAttribute(entityMetadata, attribute, services)}";
                }
            }

            return DefaultNamingService.GetNameForOptionSet(entityMetadata, optionSetMetadata, services);
        }
        
        public string GetNameForAttribute(EntityMetadata entityMetadata, AttributeMetadata attributeMetadata, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForAttribute(entityMetadata, attributeMetadata, services);
        }

        public string GetNameForEntity(EntityMetadata entityMetadata, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForEntity(entityMetadata, services);
        }

        public string GetNameForEntitySet(EntityMetadata entityMetadata, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForEntitySet(entityMetadata, services);
        }

        public string GetNameForMessagePair(SdkMessagePair messagePair, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForMessagePair(messagePair, services);
        }
        
        public string GetNameForOption(OptionSetMetadataBase optionSetMetadata, OptionMetadata optionMetadata, IServiceProvider services)
        {
            var name = DefaultNamingService.GetNameForOption(optionSetMetadata, optionMetadata, services);

            if (string.IsNullOrEmpty(name))
            {
                name = optionMetadata.Label.UserLocalizedLabel.Label;
            }

            name = EnsureValidIdentifier(name);
            name = EnsureUniqueOptionName(optionSetMetadata, name);
            return name;
        }

        public string GetNameForRelationship(EntityMetadata entityMetadata, RelationshipMetadataBase relationshipMetadata, EntityRole? reflexiveRole, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForRelationship(entityMetadata, relationshipMetadata, reflexiveRole, services);
        }

        public string GetNameForRequestField(SdkMessageRequest request, SdkMessageRequestField requestField, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForRequestField(request, requestField, services);
        }

        public string GetNameForResponseField(SdkMessageResponse response, SdkMessageResponseField responseField, IServiceProvider services)
        {
            return DefaultNamingService.GetNameForResponseField(
                response, responseField, services);
        }

        public string GetNameForServiceContext(IServiceProvider services)
        {
            return DefaultNamingService.GetNameForServiceContext(services);
        }
        #endregion
        
        /// <summary>
        /// Checks to make sure that the name begins with a valid character. If the name
        /// does not begin with a valid character, then add an underscore to the
        /// beginning of the name.
        /// </summary>
        private static string EnsureValidIdentifier(string name)
        {
            // Check to make sure that the option set begins with a word character
            // or underscore.
            const string pattern = @"^[A-Za-z_][A-Za-z0-9_]*$";

            if (!Regex.IsMatch(name, pattern))
            {
                // Prepend an underscore to the name if it is not valid.
                name = $"_{name}";
            }
            return name;
        }

        /// <summary>
        /// Checks to make sure that the name does not already exist for the OptionSet
        /// to be generated.
        /// </summary>
        private string EnsureUniqueOptionName(OptionSetMetadataBase metadata, string name)
        {
            if (_definedOptionSetNames.ContainsKey(metadata))
            {
                if (_definedOptionSetNames[metadata].ContainsKey(name))
                {
                    // Increment the number of times that an option with this name has
                    // been found.
                    ++_definedOptionSetNames[metadata][name];

                    // Append the number to the name to create a new, unique name.
                    var newName = $"{name}_{_definedOptionSetNames[metadata][name]}";

                    // Call this function again to make sure that our new name is unique.
                    return EnsureUniqueOptionName(metadata, newName);
                }
            }
            else
            {
                // This is the first time this OptionSet has been encountered. Add it to
                // the dictionary.
                _definedOptionSetNames[metadata] = new Dictionary<string, int>();
            }

            // This is the first time this name has been encountered. Begin keeping track
            // of the times we've run across it.
            _definedOptionSetNames[metadata][name] = 1;

            return name;
        }
    }
}