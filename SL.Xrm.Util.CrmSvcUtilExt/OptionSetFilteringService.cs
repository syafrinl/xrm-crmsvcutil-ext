﻿using Microsoft.Crm.Services.Utility;
using Microsoft.Xrm.Sdk.Metadata;
using SL.Xrm.Util.CrmSvcUtilExt.Filter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace SL.Xrm.Util.CrmSvcUtilExt
{
    public sealed class OptionSetFilteringService : ICodeWriterFilterService
    {
        private ICodeWriterFilterService DefaultService { get; set; }
        
        private readonly IDictionary<string, bool> _optionSets;

        private static readonly string SolutionNames = ConfigurationManager.AppSettings["solutionNames"];
        private static readonly string RequiredEntityNames = ConfigurationManager.AppSettings["requiredEntities"];

        public OptionSetFilteringService(ICodeWriterFilterService defaultService)
        {
            Trace.TraceInformation("Loading Filters...");

            DefaultService = defaultService;

            var crmService = CrmOrganizationServiceFactory.Create();
            var solutionFilter = new SolutionManager(crmService, SolutionNames, RequiredEntityNames);
            
            _optionSets = solutionFilter.OptionSetNames.ToDictionary(x => x, x => false);
        }
        
        public bool GenerateAttribute(AttributeMetadata attributeMetadata, IServiceProvider services)
        {
            return (attributeMetadata.AttributeType == AttributeTypeCode.Picklist
                || attributeMetadata.AttributeType == AttributeTypeCode.State
                || attributeMetadata.AttributeType == AttributeTypeCode.Status);
        }

        bool ICodeWriterFilterService.GenerateEntity(EntityMetadata entityMetadata, IServiceProvider services)
        {
            return DefaultService.GenerateEntity(entityMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateOption(OptionMetadata optionMetadata, IServiceProvider services)
        {
            return DefaultService.GenerateOption(optionMetadata, services);
        }

        bool ICodeWriterFilterService.GenerateOptionSet(OptionSetMetadataBase optionSetMetadata, IServiceProvider services)
        {
            if (_optionSets.ContainsKey(optionSetMetadata.Name))
            {
                if (!_optionSets[optionSetMetadata.Name])
                {
                    //mark as done to avoid duplicate
                    _optionSets[optionSetMetadata.Name] = true;
                    return true;
                }
            }

            return false;
        }

        bool ICodeWriterFilterService.GenerateRelationship(RelationshipMetadataBase relationshipMetadata, EntityMetadata otherEntityMetadata, IServiceProvider services)
        {
            return false;
        }

        bool ICodeWriterFilterService.GenerateServiceContext(IServiceProvider services)
        {
            return false;
        }
    }
}

