﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Metadata;

namespace SL.Xrm.Util.CrmSvcUtilExt.Comparer
{
    public class OptionSetComparer : IEqualityComparer<OptionSetMetadata>
    {
        public bool Equals(OptionSetMetadata x, OptionSetMetadata y)
        {
            return x.Name.Equals(y.Name);
        }

        public int GetHashCode(OptionSetMetadata obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
