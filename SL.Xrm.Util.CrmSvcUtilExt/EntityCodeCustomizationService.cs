﻿using System;
using System.Collections.Generic;
using System.CodeDom;
using System.Diagnostics;
using System.Linq;
using Microsoft.Crm.Services.Utility;
using SL.Xrm.Util.CrmSvcUtilExt.CodeCustomizer;

namespace SL.Xrm.Util.CrmSvcUtilExt
{
    public sealed class EntityCodeCustomizationService : ICustomizeCodeDomService
    {
        private List<CodeMemberProperty> QueryablePropertyList = new List<CodeMemberProperty>();

        public void CustomizeCodeDom(CodeCompileUnit codeUnit, IServiceProvider services)
        {
            Trace.TraceInformation("Customizing Code...");
            
            foreach (CodeAttributeDeclaration attribute in codeUnit.AssemblyCustomAttributes)
            {
                Trace.TraceInformation("Attribute BaseType is {0}", attribute.AttributeType.BaseType);
                if (attribute.AttributeType.BaseType == "Microsoft.Xrm.Sdk.Client.ProxyTypesAssemblyAttribute")
                {
                    codeUnit.AssemblyCustomAttributes.Remove(attribute);
                    break;
                }
            }

            foreach (CodeNamespace codeNamespace in codeUnit.Namespaces)
            {
                var types = codeNamespace.Types;
                
                foreach (CodeTypeDeclaration type in types)
                {
                    //Add Dirty Attribute Tracking on Entity classes only
                    if (type.IsClass
                        && type.CustomAttributes.Cast<CodeAttributeDeclaration>()
                                .Any(attr => attr.Name.Equals("System.Runtime.Serialization.DataContractAttribute", StringComparison.OrdinalIgnoreCase)))
                    {
                        DirtyAttributeTracking.AddAttributeTracking(type);
                        
                        foreach (CodeTypeReference baseType in type.BaseTypes)
                        {
                            if (baseType != null && baseType.BaseType.Equals("Microsoft.Xrm.Sdk.Entity", StringComparison.OrdinalIgnoreCase))
                            {
                                type.BaseTypes.Remove(baseType);
                                break;
                            }
                        }
                        type.BaseTypes.Insert(0, new CodeTypeReference("DirtyAttributeTrackableEntity"));
                    }

                    if (type.IsClass && type.Name == "XrmServiceContext")
                    {
                        type.BaseTypes.Add(new CodeTypeReference("IXrmServiceContext"));

                        foreach (CodeTypeMember member in type.Members)
                        {
                            var property = member as CodeMemberProperty;

                            if (property != null && property.Name.EndsWith("Set"))
                            {
                                QueryablePropertyList.Add(property);
                            }
                        }
                    }
                }

                if (QueryablePropertyList.Any())
                {
                    var iXrmServiceContext = new CodeTypeDeclaration("IXrmServiceContext")
                    {
                        IsInterface = true,
                        IsPartial = true
                    };

                    foreach (var member in QueryablePropertyList)
                    {
                        iXrmServiceContext.Members.Add(new CodeMemberProperty
                        {
                            Name = member.Name,
                            Type = member.Type,
                            HasGet = true
                        });
                    };

                    codeNamespace.Types.Add(iXrmServiceContext);
                }
            }
        }
    }
}