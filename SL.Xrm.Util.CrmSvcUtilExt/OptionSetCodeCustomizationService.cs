﻿using Microsoft.Crm.Services.Utility;
using System;
using System.CodeDom;
using System.Diagnostics;

namespace SL.Xrm.Util.CrmSvcUtilExt
{
    /// <summary>
    /// Create an implementation of ICustomizeCodeDomService if you want to manipulate the
    /// code dom after ICodeGenerationService has run.
    /// </summary>
    public sealed class OptionSetCodeCustomizationService : ICustomizeCodeDomService
    {
        public OptionSetCodeCustomizationService()
        {
        }

        /// <summary>
        /// Remove the unnecessary classes that we generated for entities. 
        /// </summary>
        public void CustomizeCodeDom(CodeCompileUnit codeUnit, IServiceProvider services)
        {
            Trace.TraceInformation("Entering ICustomizeCodeDomService.CustomizeCodeDom");
            Trace.TraceInformation("Number of Namespaces generated: {0}", codeUnit.Namespaces.Count);

            //foreach (CodeAttributeDeclaration attribute in codeUnit.AssemblyCustomAttributes)
            //{
            //    Trace.TraceInformation("Attribute BaseType is {0}", attribute.AttributeType.BaseType);
            //    if (attribute.AttributeType.BaseType == "Microsoft.Xrm.Sdk.Client.ProxyTypesAssemblyAttribute")
            //    {
            //        codeUnit.AssemblyCustomAttributes.Remove(attribute);
            //        break;
            //    }
            //}

            // Iterate over all of the namespaces that were generated.
            for (var i = 0; i < codeUnit.Namespaces.Count; ++i)
            {
                var types = codeUnit.Namespaces[i].Types;
                Trace.TraceInformation("Number of types in Namespace {0}: {1}", codeUnit.Namespaces[i].Name, types.Count);

                // Iterate over all of the types that were created in the namespace.
                for (var j = 0; j < types.Count; )
                {
                    // Remove the type if it is not an enum (all OptionSets are enums) or has no members.
                    if (!types[j].IsEnum || types[j].Members.Count == 0)
                    {
                        types.RemoveAt(j);
                    }
                    else
                    {
                        j += 1;
                    }
                }
            }

            Trace.TraceInformation("Exiting ICustomizeCodeDomService.CustomizeCodeDom");
        }
    }
}
