# README #

This repository contains code for Dynamics CRM 2015 Entity Model Project template.

### Change Log ###

Version 1.0.0.*

+ <ADD> Track dirty attributes to submit to server for update (Unchanged attributes will not be submitted)
+ <ADD> UpdateObject will automatically attach entity if required
+ <ADD> DeleteObject will automatically attach entity if required
+ <ADD> Only generate early bound code for specified Solutions and Entities
+ <ADD> IXrmServiceContext interface enables Unit Testing

### How do I get set up? ###

1. Build SL.Xrm.Model.Vsix project
2. Obtain SL.Xrm.Model.Vsix file from Output directory
3. Install to visual studio 2015
4. Create a new Project "Dynamics CRM 2015 Entity Model Project"
5. Edit crmsvcutil.exe.config file
6. Run GenerateEarlyBoundCode.bat to generate the early bound code

### Contact Detail ###

Syafrin Liong 
+61402504602
syafrin.liong@gmail.com